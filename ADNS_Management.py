__author__ = 'marjan falahrastegar'
import sys
import os
import commands
import csv
import tldextract

#TO GET ADNS RECORD OF A LIST OF DOMAINS CALL run_soa METHOD
 
#get second level domain of a url
def only2level(domain):
    try:

        extracted = tldextract.extract(domain)
        newDomain = "{}.{}".format(extracted.domain, extracted.suffix)
        return newDomain
    except:
        #print getError()
        return domain

#writes one row of string (data parameter) in a file(inputFile) parameter
def outFile(data, inputFile,header):
    #if you don't want header live this parameter as ""
    if header:
        if not os.path.isfile(inputFile):
            with open(inputFile, 'a') as the_file:
                the_file.write(header)
                the_file.write(os.linesep)
             
          
    with open(inputFile, 'a') as the_file:
        the_file.write(data.encode('utf-8').strip())
        the_file.write(os.linesep)

#This method gets domain name as an input 
# The output is a comma seperated string stored in a file
# The ourput shows the original domain name, ADNS record, registered email address and 2nd-level domain name of the ADNS and email.
def ns_lookup(site):
    origin = ""
    mail = ""
    p = commands.getstatusoutput('nslookup -query=soa %s' % site)
    try:
        p = p[1]
        arrp = p.split("\n")
        for a in arrp:
            if a.find("origin") > -1:
                origin = a.split("=")[1]
            elif a.find("mail") > -1:
                mail = a.split("=")[1]
        org2 = replace_generalones(origin,site)
        mail2 = replace_generalones(mail,site)
        outStr = site + "," + origin + "," + mail + "," + only2level(org2) + "," + only2level(mail2)
        outFile(outStr, "output/out_nsrecord","")
    except:
        outStr = site + ",,,,"
        outFile(outStr, "output/out_nsrecord","")
        print site + ",,,,"

#List of cdns and general hosting services identified in our dataset
def replace_generalones(ns,domain):
    general_list = ["1und1","host","cdn","dns",
                    "cloudflare","akamai","1and1",
                    "mediatemple","mailclub","moniker",
                    "hichina","dynect.net","domaincontrol.com",
                    "akam","rackspace.com","registrar-servers.com",
                    "gandi.net","name-services","softlayer.com",
                    "hyp.net","nic.ru","technorail.com",
                    "value-domain.com","loopia.se","namespace4you.de",
                    "name.com","hostmaster.netnames.net"]
    for g in general_list:
        if ns.find(g) > -1:
            return domain #if it is general then use the main domain instead of ns record
    return ns



#read an input file into a dictionary format
##input file MUST have a header row calling "url"
#After header, each row should have one domain name
def run_soa(inputFile):
    with open(inputFile, "rb") as theFile:
        line = csv.DictReader(theFile)
        for l in line:
            ns_lookup(l["url"])
#ns_lookup(l["domain"])

input_file = ""#path to the file containing list of websites
run_soa(input_file)
